require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const saltRounds = 10;
const cors = require("cors");

const mysqlConnection = require("./connection");
const userRoutes = require("./routes/users");
const deptRoutes = require("./routes/departments");
const peopleRoutes = require("./routes/peoples");
const joinRoutes = require("./routes/joins");
const authRoutes = require("./routes/auth");
const loginRoutes = require("./routes/login");
const postsRoutes = require("./routes/posts");
const isLoggedinRoutes = require("./routes/isLoggedin");
const doLogoutRoutes = require("./routes/doLogout");

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use("/people", peopleRoutes);
app.use("/users", userRoutes);
app.use("/dep", deptRoutes);
app.use("/join", joinRoutes);
app.use("/auth", authRoutes);
app.use("/login", loginRoutes);
app.use("/posts", postsRoutes);
app.use("/isLoggedin", isLoggedinRoutes);
app.use("/doLogout", doLogoutRoutes);

app.listen(5000);
