const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

//JOIN user with people
Router.get("/joinuser", (req, res) => {
  mysqlConnection.query(
    "SELECT people.Name AS employee_name, user.mail AS mail FROM people JOIN user ON people.user_id = user.user_id",
    [req.body],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//JOIN dep with people
Router.get("/joindep", (req, res) => {
  mysqlConnection.query(
    "SELECT people.Name AS people, department.dep_id AS depId FROM people JOIN department ON people.dep_id = department.dep_id",
    [req.body],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = Router;
