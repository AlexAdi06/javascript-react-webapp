const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

//insert one in the table user
Router.post("/", (req, res, next) => {
  const username = req.body.username;
  const mail = req.body.mail;
  const password = req.body.password;

  bcrypt.hash(password, saltRounds, (err, hash) => {
    mysqlConnection.query(
      "INSERT INTO user (username, mail, password) VALUES(?,?,?)",
      [username, mail, hash],
      (err, results, fields) => {
        if (!err) {
          res.send("Registration with success,");
        } else {
          console.log(err);
        }
      }
    );
  });
});

module.exports = Router;
