const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

Router.post("/", (req, res) => {
  const { mail, password } = req.body;
  mysqlConnection.query(
    "SELECT * FROM user WHERE mail = ?",
    [mail],
    (err, results, fields) => {
      if (err) {
        res.status(500).send({ message: "Error on your query" });
      }
      if (!results) {
        res.status(400).send({
          message: "Incorrect email!",
          success: false,
        });
      } else {
        bcrypt.compare(password, results[0].password).then((verified) => {
          if (!verified) {
            res.status(400).send({
              message: "Incorrect password!",
              success: false,
            });
          } else {
            var accesstoken = jwt.sign(
              { id: results[0].user_id, mail: results[0].mail },
              process.env.ACCESS_TOKEN_SECRET,
              {
                expiresIn: "30m",
              }
            );
            let user = {
              mail: mail,
              accesstoken: accesstoken,
              success: true,
            };
            success = true;
            res.status(200).send(user);
          }
        });
      }
    }
  );
});

module.exports = Router;
