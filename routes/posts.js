const express = require("express");
const Router = express.Router();
const verify = require("./verifyToken");

Router.get("/", verify, (req, res) => {
  res.json({
    posts: {
      title: "One random to verify jwt post",
      description: "You don't have access to this!",
    },
  });
});

module.exports = Router;
