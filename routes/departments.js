const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

//get all departments
Router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM department", (err, rows, fields) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//get one from the department table by ID
Router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM department WHERE dep_id = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//delete one from the department by ID
Router.delete("/:id", (req, res) => {
  mysqlConnection.query(
    "DELETE FROM department WHERE dep_id = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Deleted succesfully!");
      } else {
        console.log(err);
      }
    }
  );
});

//insert one in the table department
Router.post("/", (req, res) => {
  mysqlConnection.query(
    "INSERT INTO department SET ?",
    req.body,
    (err, rows, fields) => {
      if (!err) {
        res.send("Inserted with success");
      } else {
        console.log(err);
      }
    }
  );
});

//update one row in the table department
Router.put("/:id", (req, res) => {
  mysqlConnection.query(
    "UPDATE department SET ? WHERE dep_id = ?",
    [req.body, req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Updated successfully!");
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = Router;
