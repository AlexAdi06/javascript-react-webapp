const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

//get all users
Router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM user", (err, rows, fields) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//get one from the user by ID
Router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM user WHERE user_id = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//delete one from the user by ID
Router.delete("/:id", (req, res) => {
  mysqlConnection.query(
    "DELETE FROM user WHERE user_id = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Deleted succesfully!");
      } else {
        console.log(err);
      }
    }
  );
});

//update one row in the user table
Router.put("/:id", (req, res) => {
  mysqlConnection.query(
    "UPDATE user SET ? WHERE user_id = ?",
    [req.body, req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Updated successfully!");
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = Router;
