const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

//get all people
Router.get("/", (req, res) => {
  mysqlConnection.query("SELECT * FROM people", (err, rows, fields) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

//get one from the people by ID
Router.get("/:id", (req, res) => {
  mysqlConnection.query(
    "SELECT * FROM people WHERE ID = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//delete one from the people by ID
Router.delete("/:id", (req, res) => {
  mysqlConnection.query(
    "DELETE FROM people WHERE ID = ?",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Deleted succesfully!");
      } else {
        console.log(err);
      }
    }
  );
});

//insert one in the table people
Router.post("/", (req, res) => {
  mysqlConnection.query(
    "INSERT INTO people SET ?",
    req.body,
    (err, rows, fields) => {
      if (!err) {
        res.send("Inserted with success");
      } else {
        console.log(err);
      }
    }
  );
});

//update one row in the table people
Router.put("/:id", (req, res) => {
  mysqlConnection.query(
    "UPDATE people SET ? WHERE ID = ?",
    [req.body, req.params.id],
    (err, rows, fields) => {
      if (!err) {
        res.send("Updated successfully!");
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = Router;
