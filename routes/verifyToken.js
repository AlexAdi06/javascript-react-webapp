const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  const header = req.header["auth"];

  if (header == null) return res.status(401).send("Access denied!");

  try {
    const verified = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    res.status(400).send("Invalid Token!!");
  }
};
