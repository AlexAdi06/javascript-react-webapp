const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

Router.post("/", (req, res) => {
  if (req.user_id) {
    let cols = [req.user_id];
    mysqlConnection.query(
      "SELECT * FROM user WHERE user_id = ? LIMIT 1",
      cols,
      (err, data, fields) => {
        if (data && data.length === 1) {
          res.json({
            success: true,
            mail: data[0].mail,
          });
          return true;
        } else {
          res.json({
            success: false,
          });
        }
      }
    );
  }
});

module.exports = Router;
