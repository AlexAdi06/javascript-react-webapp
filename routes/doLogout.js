const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");

Router.post("/", (req, res) => {
  if (req.user_id) {
    req.destroy();
    res.json({
      success: true,
    });
    return true;
  } else {
    console.log("doLogout was called");
    res.json({
      success: true,
    });
    return false;
  }
});

module.exports = Router;
