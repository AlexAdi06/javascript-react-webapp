-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: aug. 06, 2020 la 10:38 AM
-- Versiune server: 10.4.13-MariaDB
-- Versiune PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `dbexample`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `department`
--

CREATE TABLE `department` (
  `dep_id` int(11) NOT NULL,
  `dep_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`) VALUES
(101, 'IT'),
(102, 'HR'),
(103, 'Finance');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `people`
--

CREATE TABLE `people` (
  `ID` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Salary` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `people`
--

INSERT INTO `people` (`ID`, `Name`, `Salary`, `dep_id`, `user_id`) VALUES
(1, 'Alexandru', 2000, 102, 1),
(2, 'Ionela', 3000, 103, 8),
(3, 'Maria', 3200, 101, 2),
(4, 'Sofia', 3300, 101, 3),
(5, 'Sabin', 4000, 102, 5),
(6, 'Petru', 2700, 101, 6),
(7, 'Mirel', 5200, 103, 7),
(8, 'Ovidiu', 6300, 103, 9),
(9, 'Nicolae', 2300, 101, 4),
(10, 'Herman', 3300, 101, 10),
(11, 'Florin', 5700, 102, 11);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `password` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Eliminarea datelor din tabel `user`
--

INSERT INTO `user` (`user_id`, `username`, `mail`, `password`) VALUES
(1, 'Alexandru', 'alexandru06@people.com', '$2b$10$/d8ZHJzlgG/uV/0JQR3XXuUFMHx9Zkldp5ipLMmwGNGC7.lYf9YsC'),
(2, 'Maria', 'maria08@people.com', '$2b$10$PXGLckDlgrK35z5MtSW6dORS8Dwsxf8VU.odDmc1l9c7JmISoutX.'),
(3, 'Sofia', 'sofia12@people.com', '$2b$10$SZZTohfLQCgpXu.4xvjoau0cHBStdWR43BfnoWKnC3ON7VG/foS1K'),
(4, 'Nicolae', 'nicolae34@people.com', '$2b$10$2zIk4/b01md/DA0YFU7MNeScsF27VVX7REr5tHsRNKUP.c346NelK'),
(5, 'Sabin', 'sabin23@people.com', '$2b$10$XRkYg.cZ01c7KpIZmnXQouxY1lwzhx4HZswSUGFd1vDeZ1Bys2QhW'),
(6, 'Petru', 'petru23@people.com', '$2b$10$p8lZDMERNwzRcd.5NWSSP.xoEWupa17uZPy7SCfTbCt6u.tBdQ9oG'),
(7, 'Mirel', 'mirel02@people.com', '$2b$10$3sMdkLb7ijC03oGKmgpfVOkc2rM9NEIFRyiVllOwAF5ASd52YgydK'),
(8, 'Ionela', 'ionela17@people.com', '$2b$10$qqEFSqPMmzcL7/HtGYDPQusYhJC8ZQ4HPGezEKGsgiR4wPI1Kz8R6'),
(9, 'Ovidiu', 'ovidiu31@people.com', '$2b$10$g2Xvz66yhFMoK94yxkp1UeEwypdPYklLMeoEPuYklHwoGYSDiMFOG'),
(10, 'Herman', 'herman28@people.com', '$2b$10$8172iV1tvnWl7HsVA9Kh2eEummZH4N6MSjYE0yUrG2fYnqDparzb.'),
(11, 'Florin', 'florin08@people.com', '$2b$10$d6XMPQyVsns5AKcTGwAq0uCRze/Qe8ac3hSkYdJSYGrPBQCRYfT0S');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexuri pentru tabele `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `dep_id` (`dep_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexuri pentru tabele `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQUE` (`mail`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `department`
--
ALTER TABLE `department`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT pentru tabele `people`
--
ALTER TABLE `people`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pentru tabele `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
