import { extendObservable } from "mobx";

class UserStore {
  constructor() {
    extendObservable(this, {
      loading: true,
      isLoggedIn: false,
      isReg: false,
      mail: "",
      forLogin: false,
      forReg: false,
    });
  }
}

export default new UserStore();
