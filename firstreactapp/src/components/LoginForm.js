import React from "react";
import InputField from "./InputField";
import SubmitButton from "./SubmitButton";
import UserStore from "../store/UserStore";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mail: "",
      password: "",
      buttonDisabled: false,
    };
  }

  setInputValue(property, val) {
    val = val.trim();
    if (val.length > 30) {
      return;
    } else {
      this.setState({
        [property]: val,
      });
    }
  }

  resetLoginForm() {
    this.setState({
      mail: "",
      password: "",
      buttonDisabled: false,
    });
  }

  async doLogin() {
    if (!this.state.mail) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    this.setState({
      buttonDisabled: true,
    });
    try {
      let res = await fetch("/login", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          mail: this.state.mail,
          password: this.state.password,
        }),
      });
      let result = await res.json();
      if (result && result.success) {
        UserStore.isLoggedIn = true;
        UserStore.forLogin = false;
        UserStore.mail = result.mail;
      } else if (result && result.success === false) {
        this.resetLoginForm();
        alert(result.msg);
      }
    } catch (e) {
      console.log(e);
      this.resetLoginForm();
    }
  }

  render() {
    return (
      <div className="loginForm">
        Log in
        <InputField
          type="text"
          placeholder="email"
          value={this.state.mail ? this.state.mail : ""}
          onChange={(val) => this.setInputValue("mail", val)}
        />
        <InputField
          type="password"
          placeholder="Password"
          value={this.state.password ? this.state.password : ""}
          onChange={(val) => this.setInputValue("password", val)}
        />
        <SubmitButton
          text="Login"
          disabled={this.state.buttonDisabled}
          onClick={() => this.doLogin()}
        />
      </div>
    );
  }
}
export default LoginForm;
