import React from "react";
import InputField from "./InputField";
import SubmitButton from "./SubmitButton";
import UserStore from "../store/UserStore";
import { set } from "lodash";

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mail: "",
      password: "",
      buttonDisabled: false,
    };
  }

  setInputValue(property, val) {
    val = val.trim();
    if (val.length > 30) {
      return;
    } else {
      this.setState({
        [property]: val,
      });
    }
  }

  resetRegForm() {
    this.setState({
      username: "",
      mail: "",
      password: "",
      buttonDisabled: false,
      isReg: true,
      forReg: false,
    });
  }

  async doRegister() {
    UserStore.isReg = true;
    UserStore.forReg = false;
    if (!this.state.username) {
      return;
    }
    if (!this.state.mail) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    this.setState({
      buttonDisabled: true,
      isReg: true,
    });
    try {
      let res = await fetch("/auth", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: this.state.username,
          mail: this.state.mail,
          password: this.state.password,
        }),
      });
      let result = await res.json();
      if (result) {
        UserStore.forReg = false;
      } else if (result === false) {
        this.resetRegForm();
        alert(result.msg);
      }
    } catch (e) {
      console.log(e);
      this.resetRegForm();
    }
  }

  render() {
    return (
      <div className="registerForm">
        Register
        <InputField
          type="text"
          placeholder="username"
          value={this.state.username ? this.state.username : ""}
          onChange={(val) => this.setInputValue("username", val)}
        />
        <InputField
          type="text"
          placeholder="email"
          value={this.state.mail ? this.state.mail : ""}
          onChange={(val) => this.setInputValue("mail", val)}
        />
        <InputField
          type="password"
          placeholder="Password"
          value={this.state.password ? this.state.password : ""}
          onChange={(val) => this.setInputValue("password", val)}
        />
        <SubmitButton
          text="Register"
          disabled={this.state.buttonDisabled}
          onClick={() => this.doRegister()}
        />
      </div>
    );
  }
}
export default RegisterForm;
