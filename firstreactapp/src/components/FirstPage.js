import React from "react";
import SubmitButton from "./SubmitButton";
import UserStore from "../store/UserStore";

class FirstPage extends React.Component {
  render() {
    return (
      <div className="loginForm">
        <SubmitButton
          text="Login"
          onClick={() => (UserStore.forLogin = true)}
        />

        <SubmitButton
          text="Register"
          onClick={() => (UserStore.forReg = true)}
        />
      </div>
    );
  }
}

export default FirstPage;
