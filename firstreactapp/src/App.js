import React from "react";
import { observer } from "mobx-react";
import UserStore from "./store/UserStore";
import LoginForm from "./components/LoginForm";
import SubmitButton from "./components/SubmitButton";
import RegisterForm from "./components/RegisterForm";

import "./App.css";
import FirstPage from "./components/FirstPage";

class App extends React.Component {
  async componentDidMount() {
    try {
      let res = await fetch("/isLoggedIn", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();

      if (result && result.success) {
        UserStore.loading = false;
        UserStore.isLoggedIn = true;
        UserStore.isReg = true;
        UserStore.mail = result.mail;
      } else {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      }
    } catch (err) {
      UserStore.loading = false;
      UserStore.isLoggedIn = false;
    }
  }
  async doGetAll() {
    try {
      let res = await fetch("/users", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();
      if (result) {
        console.log("result  e TRUEE");
      }
    } catch (err) {
      console.log(err);
    }
  }

  async doLogout() {
    try {
      let res = await fetch("/doLogout", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();

      if (result && result.success) {
        UserStore.isLoggedIn = false;
        UserStore.isReg = false;
        UserStore.mail = "";
      }
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    if (UserStore.isLoggedIn) {
      return (
        <div className="App">
          <div className="container">
            Welcome {UserStore.mail}
            <SubmitButton
              text={"LOGOUT"}
              disabled={false}
              onClick={() => this.doLogout()}
            />
            <SubmitButton
              text={"See the users"}
              disabled={false}
              onClick={() => this.doGetAll()}
            />
          </div>
        </div>
      );
    }
    if (UserStore.isReg) {
      return (
        <div className="App">
          <div className="container">
            Hi {UserStore.mail}, your registration was successful!
            <SubmitButton
              text={"to Main page"}
              disabled={false}
              onClick={() => this.doLogout()}
            />
          </div>
        </div>
      );
    }
    if (UserStore.forLogin) {
      return (
        <div className="App">
          <div className="container">
            <LoginForm />
          </div>
        </div>
      );
    }
    if (UserStore.forReg) {
      return (
        <div className="App">
          <div className="container">
            <RegisterForm />
          </div>
        </div>
      );
    }
    return (
      <div className="App">
        <div className="container">
          <FirstPage />
        </div>
      </div>
    );
  }
}

export default observer(App);
